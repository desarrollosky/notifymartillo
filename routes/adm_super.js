const connection = require('../config/db');
const { isLoggedIn } = require('../config/auth');

const index = "/notifyMartillo/";

module.exports = function (express, globals) {
    const router = express.Router();
    routes = {
        index: function (req, res, next) {
            content = {
                title: 'Administrar supervisores',
                globals: encodeURIComponent(JSON.stringify(globals)),
            };
            res.render('administrar_supervisores/index', content);
        },
        supervisores: function (req, res, next) {

            let sql = "SELECT id, idRol, nombre, apellido, username, estado FROM usuario WHERE idRol='2';";

            let query = connection.query(sql, (err, results) => {
                if (err) {
                    throw err;
                } else {
                    if (results.length != 0) {
                        res.render('administrar_supervisores/index', {
                            supervisores: results
                        });
                    } else {
                        res.render('administrar_supervisores/index', {
                            supervisores: 'vacio'
                        });
                    }
                }
            });
        }
    };

    router.get('/index', isLoggedIn, (routes.index, routes.supervisores));
    return router;
};

module.exports.actdes_supervisor = function (req, res) {
    res.setHeader('Content-type', 'text/plain');
    let id = req.body.id;
    let checked = req.body.checked;
    let userRol = req.user.idRol;

    var data = {
        "class": "",
        "msj": ""
    };

    if (userRol === 1) {
        if (checked === 'true') {
            connection.query("UPDATE usuario SET estado = '1' WHERE id = ?;", [id], function (err, result) {
                if (err) {
                    data["class"] = 'alert-danger';
                    data["msj"] = 'Ocurrió un error en el sistema.';
                    res.json(data);
                } else {
                    data["class"] = 'alert-success';
                    data["msj"] = 'Se modificó el acceso del usuario correctamente.';
                    res.json(data);
                }
            });
        } else {
            connection.query("UPDATE usuario SET estado = '0' WHERE id = ?;", [id], function (err, result) { 
                if (err) {
                    data["class"] = 'alert-danger';
                    data["msj"] = 'Ocurrió un error en el sistema.';
                    res.json(data);
                } else {
                    data["class"] = 'alert-success';
                    data["msj"] = 'Se modificó el acceso del usuario correctamente.';
                    res.json(data);
                }
            });
        }
    } else {
        data["class"] = 'alert-danger';
        data["msj"] = 'Se necesitan permisos para realizar modificaciones.';
        res.json(data);
    }
}