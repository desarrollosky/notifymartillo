const { isLoggedIn } = require('../config/auth');
const connection = require('../config/db');

module.exports = function (express, globals) {
    const router = express.Router();
    routes = {
        index: function (req, res, next) {
            content = {
                title: 'Reportes',
                globals: encodeURIComponent(JSON.stringify(globals)),
            };
            res.render('reportes/index', content);
        }
    };
    router.get('/index', isLoggedIn, routes.index);

    return router;
};

module.exports.estadisticas_grupos = function (req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot2 = req.body.id;
    var data = {};

    connection.query("SELECT count(*) as urgente from notificacion WHERE estado=1; SELECT count(*) as proceso from notificacion WHERE estado=2; SELECT count(*) as concluido from notificacion WHERE estado=3;", function (err, rows, fields) {
        if (err) { throw err; } else {

            data = {
                urgente: rows[0],
                proceso: rows[1],
                concluido: rows[2]
            }
            res.json(data);
        }
    });
}

module.exports.graficas_barra = function (req, res) {

    /*
    Alarma SOS~
    3.2 Pérdida de posición por 5 días~
    [F+JD] Bloqueo ciclo 30/30 por Jammer~
    [F+JD] Exceso de velocidad 93 km/h~
    4. Recarga de combustible~
    5. Descarga de combustible~ */

    res.setHeader('Content-type', 'text/plain');
    let inicio = req.body.inicio;
    let final = req.body.final;
    var sql = "";
    var data = {
        "sos": "",
        "perdidaPos": "",
        "jammer": "",
        "excvelocd": "",
        "recombust": "",
        "descombust": ""
    };

    if ((inicio === undefined || inicio === null || inicio === "") && (final === undefined || final === null || final === "")) {
        //Presiones SOS
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='Alarma SOS~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Perdida de posicion
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='3.2 Pérdida de posición por 5 días~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Jammer
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='[F+JD] Bloqueo ciclo 30/30 por Jammer~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Exceso de velocidad
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='[F+JD] Exceso de velocidad 93 km/h~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Recarga de combustible
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='4. Recarga de combustible~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Descarga de combustible
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='5. Descarga de combustible~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";


        connection.query(sql, function (err, rows, fields) {
            if (err) { throw err; } else {
                data["sos"] = rows[0];
                data["perdidaPos"] = rows[1];
                data["jammer"] = rows[2];
                data["excvelocd"] = rows[3];
                data["recombust"] = rows[4];
                data["descombust"] = rows[5];
                res.json(data);
            }
        });
    } else {
        inicio = inicio + " 00:00:00";
        final = final + " 23:59:59";

        //Presiones SOS
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='Alarma SOS~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Perdida de posicion
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='3.2 Pérdida de posición por 5 días~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Jammer
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='[F+JD] Bloqueo ciclo 30/30 por Jammer~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Exceso de velocidad
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='[F+JD] Exceso de velocidad 93 km/h~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Recarga de combustible
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='4. Recarga de combustible~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Descarga de combustible
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='5. Descarga de combustible~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";

        connection.query(sql, [inicio, final, inicio, final, inicio, final, inicio, final, inicio, final, inicio, final, inicio, final, inicio, final], function (err, rows, fields) {
            if (err) { throw err; } else {
                data["sos"] = rows[0]; 
                data["perdidaPos"] = rows[1];
                data["jammer"] = rows[2]; 
                data["excvelocd"] = rows[3];
                data["recombust"] = rows[4];
                data["descombust"] = rows[5];
                res.json(data);
            }
        });
    }
}

module.exports.promedio_cierre = function (req, res) {
    res.setHeader('Content-type', 'text/plain');
    var sql = "";
    var data = {
        "cierre": "",
    };

    sql += "SELECT COUNT(*) totalN, ROUND(((SELECT COUNT(estado) concluidas FROM notificacion WHERE estado='3')/(SELECT COUNT(*) totalnotif from notificacion) * 100),2) AS promedioCierre FROM notificacion;";

    connection.query(sql, function (err, rows, fields) {
        if (err) { throw err; } else {
            data["cierre"] = rows[0];
            res.json(data);
        }
    });
}

module.exports.promedio_asistencia = function (req, res) {
    res.setHeader('Content-type', 'text/plain');
    var sql = "";
    var data = {
        "asistencia": "",
    };

    sql += "SELECT COUNT(*) totalN, ROUND(((SELECT COUNT(DISTINCT(idNotificacion)) asistencia FROM comentario)/(SELECT COUNT(*) totalnotif from notificacion) * 100),2) AS promedioAsist FROM notificacion";

    connection.query(sql, function (err, rows, fields) {
        if (err) { throw err; } else {
            data["asistencia"] = rows[0];
            res.json(data);
        }
    });
}