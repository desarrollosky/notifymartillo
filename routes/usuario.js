const connection = require('../config/db');
const multer = require('multer');
const shortid = require('short-id');
const { isLoggedIn } = require('../config/auth');
const bcrypt = require('bcryptjs');

module.exports = function (express, globals) {
    const router = express.Router();

    routes = {
        index: function (req, res, next) {
            content = {
                title: 'Usuarios',
                globals: encodeURIComponent(JSON.stringify(globals)),
            };
            res.render('usuarios/index', content);
        },
        show: function (req, res, next) {
            content = {
                title: 'Usuarios',
                globals: encodeURIComponent(JSON.stringify(globals)),
            };
            res.render('usuarios/show', content);
        },
        edit: function (req, res, next) {
            content = {
                title: 'Usuario',
                globals: encodeURIComponent(JSON.stringify(globals)),
            };

            res.render('usuarios/edit', content);
        }
    };

    router.get('/index', isLoggedIn, routes.index);
    router.get('/show/:id', isLoggedIn, routes.show);
    router.get('/edit/:id', isLoggedIn, (routes.edit));

    return router;
};

module.exports.procesarImagen = (req, res, next) => {
    upload(req, res, function (error) {
        if (error) {
            if (error instanceof multer.MulterError) {
                if (error.code === 'LIMIT_FILE_SIZE') {
                    req.flash('error', 'El tamaño de la imagen es demasiado grande, debe ser menor a 1000Kb');
                    res.redirect('/notifyMartillo/usuarios/edit/5');
                } else {
                    console.log('error', error.message);
                }
            } else {
                console.log('error express', error.message);
            }
            return;
        } else {
            next();
        }
    });
}

const confMulter = {
    storage: fileStorage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, __dirname + '/../public/img/uploadsImg/');
        },
        filename: (req, file, cb) => {
            const extension = file.mimetype.split('/')[1];
            cb(null, `${shortid.generate()}.${extension}`);
        }
    }),
    fileFilter(req, file, cb) {
        if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg') {
            cb(null, true);
        } else {
            cb(null, false);
        }
    },
    limits: { fileSize: 1000000 },
}

const upload = multer(confMulter).single('imagen');

module.exports.subirImagen = function (req, res) {

    let userID = req.user.id;
    let nombre = req.body.nombre;
    let apellido = req.body.apellido;
    let nameImg = req.body.inputNameFile;

    if (nameImg === "") {
        connection.query("UPDATE usuario SET nombre=?, apellido=? WHERE id=?;", [nombre, apellido, userID], function (err, rows) {
            if (err) { throw err; } else {
                if (rows.affectedRows == 1) {
                    req.flash('success', 'Los datos de perfil se guardaron correctamente');
                    res.redirect('/notifyMartillo/usuarios/edit/5');
                } else {
                    req.flash('error', 'Se encontró un error en el proceso');
                    res.redirect('/notifyMartillo/usuarios/edit/5');
                }
            }
        });
    } else {
        let imagenn = req.file.filename;

        connection.query("UPDATE usuario SET nombre=?, apellido=?, img_perfil=? WHERE id=?;", [nombre, apellido, imagenn, userID], function (err, rows) {
            if (err) { throw err; } else {
                if (rows.affectedRows == 1) {
                    req.flash('success', 'Los datos de perfil se guardaron correctamente');
                    res.redirect('/notifyMartillo/usuarios/edit/5');
                } else {
                    req.flash('error', 'Se encontró un error en el proceso');
                    res.redirect('/notifyMartillo/usuarios/edit/5');
                }
            }
        });
    }
}

module.exports.actPass = function (req, res) {
    res.setHeader('Content-type', 'text/plain');

    let userID = req.user.id;
    let oldpass = req.body.oldpass;
    let password = req.body.password;

    var data = {
        "msj": ""
    };

    connection.query("SELECT * FROM usuario WHERE id=?;", [userID], function (err, rows) {
        if (err) { throw err; }
        else {
            if (rows.length > 0) {
                const user = rows[0];

                bcrypt.compare(oldpass, user.password, function (err, isMatch) {
                    if (err) {
                        throw err;
                    } else if (isMatch) {
                        //Encrypt
                        const saltRounds = 10;
                        bcrypt.genSalt(saltRounds, function (err, salt) {
                            if (err) {
                                throw err
                            } else {
                                bcrypt.hash(password, salt, function (err, hash) {
                                    if (err) {
                                        throw err
                                    } else {
                                        connection.query("UPDATE usuario SET password=? WHERE id=?;", [hash, userID], function (err, rows, fields) {
                                            if (err) {
                                                data["msj"] = 'Se encontró un error en el proceso';
                                                data["status"] = 'error';
                                                res.json(data);
                                            } else {
                                                data["msj"] = 'La contraseña fue actualizada correctamente';
                                                data["status"] = 'correct';
                                                res.json(data);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        data["msj"] = 'La contraseña actual no coincide, por favor escribir correctamente la contraseña para actualizar.';
                        data["status"] = 'error';
                        res.json(data);
                    }
                });
            } else {
                data["msj"] = 'Ocurrio un error en el sistema';
                data["status"] = 'error';
                res.json(data);
            }
        }
    });
}