const connection = require('../config/db');
const { isLoggedIn } = require('../config/auth');
const bcrypt = require('bcryptjs');

const index = "/notifyMartillo/";

module.exports = function (express, globals) {
    const router = express.Router();
    routes = {
        index: function (req, res, next) {
            content = {
                title: 'Crear supervisor',
                globals: encodeURIComponent(JSON.stringify(globals)),
            };
            res.render('crear_supervisor/index', content);
        }
    };

    router.get('/index', isLoggedIn, (routes.index));
    return router;
};

module.exports.crear_supervisor = function (req, res) {
    res.setHeader('Content-type', 'text/plain');

    let nombre = req.body.nombre;
    let apellido = req.body.apellido;
    let correo = req.body.correo;
    let password = req.body.password;

    var data = {
        "msj": "",
        "status": ""
    };

    connection.query("SELECT * FROM usuario WHERE username=?;", [correo], function (err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length > 0) {
                data["msj"] = 'Ya existe un usuario asignado a este correo';
                data["status"] = 'error';
                res.json(data);
            } else {
                //Encrypt
                const saltRounds = 10;
                bcrypt.genSalt(saltRounds, function (err, salt) {
                    if (err) {
                        throw err
                    } else {
                        bcrypt.hash(password, salt, function (err, hash) {
                            if (err) {
                                throw err
                            } else {

                                connection.query("INSERT INTO usuario (idRol, idEmpresa, nombre, apellido, username, password, img_perfil, estado) VALUES ('2', '1', ?, ?, ?, ?, 'default.jpg', '1');", [nombre, apellido, correo, hash], function (err, rows, fields) {
                                    if (err) {
                                        data["msj"] = 'Se encontró un error en el proceso';
                                        data["status"] = 'error';
                                        res.json(data);
                                    } else {
                                        data["msj"] = 'EL usuario fue creado correctamente, el monitorista ya puede ingresar a su cuenta.';
                                        data["status"] = 'correct';
                                        res.json(data);
                                    }
                                });
                            }
                        })
                    }
                });
            }
        }
    });

}