const express = require('express');
const session = require('express-session');
const MySqlStore = require('express-mysql-session')(session);
const flash = require('connect-flash');
const passport = require('passport');
const db = require('./config/db.js');
const globals = require("./config/globalConsts");
const path = require('path');
const bodyParser = require('body-parser'); 
const fetch = require('node-fetch');
const exphbs = require('express-handlebars');
const FormData = require('form-data');
const { isNotLoggedIn } = require('./config/auth');
const { isLoggedIn } = require('./config/auth');
const nodemailer = require('nodemailer');

const app = express();
//const port = process.env.PORT || 3001; 
const port = process.env.PORT || 4004;

require('./config/passport');

// accept url encoded
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.json({ limit: '1mb' }));

// accept json 
app.use(bodyParser.json());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.engine('hbs', exphbs({
    extname: '.hbs',
    layoutsDir: 'views/layouts',
    partialsDir: 'views/partials',
    defaultLayout: 'default'
}));
 
app.use(session({
    secret: 'SkyGestor',
    resave: true,
    saveUninitialized: true
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

//Global Variables
app.use((req, res, next) => {
    app.locals.success = req.flash('success');
    app.locals.error = req.flash('error');
    app.locals.warning = req.flash('warning');
    app.locals.user = req.user;
    app.locals.page = 0;
    next();
});

app.use(express.static(path.join(__dirname, 'public')));

//RUTAS
const usuario = require('./routes/usuario.js')(express, globals);
const reportes = require('./routes/reportes.js')(express, globals);
const notificacion = require('./routes/notificacion')(express, globals, port);
const concluidos = require('./routes/concluidos')(express, globals);
const descargar = require('./routes/descargar')(express, globals);
const skydash = require('./routes/skydash.js')(express, globals);
const papelera = require('./routes/papelera.js')(express, globals);
const proceso = require('./routes/proceso.js')(express, globals);
const adm_monit = require('./routes/adm_monit.js')(express, globals);
const adm_super = require('./routes/adm_super.js')(express, globals);
const crear_monit = require('./routes/crear_monit.js')(express, globals);
const crear_super = require('./routes/crear_super.js')(express, globals);
const ayuda = require('./routes/ayuda.js')(express, globals);
const login = require('./routes/login.js')(express);

//APLICACION DE RUTAS
const index = "/notifyMartillo/"; 
app.use(index + 'login', login);
app.use(index + 'usuarios', usuario);
app.use(index + 'reportes', reportes);
app.use(index + 'notificaciones', notificacion);
app.use(index + 'concluidos', concluidos);
app.use(index + 'descargar', descargar);
app.use(index + 'skydash', skydash);
app.use(index + 'papelera', papelera); 
app.use(index + 'proceso', proceso);
app.use(index + 'administrar_monitoristas', adm_monit);
app.use(index + 'administrar_supervisores', adm_super);
app.use(index + 'crear_monitorista', crear_monit);
app.use(index + 'crear_supervisor', crear_super);
app.use(index + 'ayuda', ayuda);
app.listen(port);

//FUNCIONES
const notif = require('./routes/notificacion');
const concluid = require('./routes/concluidos');
const userr = require('./routes/usuario');
const descarga = require('./routes/descargar');
const signin = require('./routes/login');
const deletePapelera = require('./routes/papelera');
const grafica = require('./routes/reportes');
const enproceso = require('./routes/proceso');
const crearMonit = require('./routes/crear_monit');
const crearSup = require('./routes/crear_super');
const adminMonit = require('./routes/adm_monit');
const adminSuper = require('./routes/adm_super');

app.post('/index', notif.traer_notificaciones);
app.post('/submit', isLoggedIn, notif.post_agregarComent);
app.post('/historial', notif.post_historialComent);
app.post('/insertNotif', notif.insertar_notificaciones);
app.post('/deleteAllnotify', notif.enviarNoti_papelera);

app.post('/concluidos_submit', isLoggedIn, concluid.post_agregarComent);
app.post('/concluidos_historial', isLoggedIn, concluid.post_concl_historialComent);
app.post('/borrar_notificacion', isLoggedIn, concluid.borrar_notificacion);
app.post('/sendTrash', isLoggedIn, concluid.enviar_papelera);
app.post('/notconcl', concluid.traer_notificaciones_concl);

app.post('/notEnProceso', enproceso.traer_notificaciones_proc);
app.post('/proceso_historial', isLoggedIn, enproceso.post_process_historialComent);
app.post('/proceso_submit', isLoggedIn, enproceso.post_cambiarEst);
app.post('/borrar_notif', isLoggedIn, enproceso.borrar_notificacion);

app.post('/act_imagen', isLoggedIn, userr.procesarImagen, userr.subirImagen);
app.post('/act_pass', isLoggedIn, userr.actPass);
app.post('/exportExcel', isLoggedIn, descarga.downloadExcel);
app.post('/sessionStart', isNotLoggedIn, signin.sessionStart);
app.post('/send_mail', isNotLoggedIn, signin.sendMail);

app.post('/papelera_borrar_not', deletePapelera.borrar_notif);
app.post('/deleteAll', deletePapelera.delete_All);
app.post('/notpapelera', deletePapelera.traer_notificaciones_pap);
app.post('/papelera_historial', deletePapelera.mostrarHistorial);

app.post('/estadisticasEst', grafica.estadisticas_grupos);
app.post('/graficabarra', grafica.graficas_barra);
app.post('/promedioConcl', grafica.promedio_cierre);
app.post('/promedioAsis', grafica.promedio_asistencia);

app.post('/crear_monitorista', isLoggedIn, crearMonit.crear_monitorista);
app.post('/crear_supervisor', isLoggedIn, crearSup.crear_supervisor);

app.post('/saveMonit', isLoggedIn, adminMonit.actdes_monitorista);
app.post('/saveSuper', isLoggedIn, adminSuper.actdes_supervisor);

app.get('/logout', (req, res) => {
    req.logOut();
    res.redirect(index + 'login/signin');
});

app.get(index +'login/signin/app/logo.png', (req, res) => {
    res.sendFile('app/logo.png', { root:__dirname + '/public/' });
});

app.post('/countUrgProc', (req, res) => {
    res.setHeader('Content-type', 'text/plain');

    var data = {
        "resUrg": "",
        "resProc": "" 
    };

    db.query("SELECT COUNT(*) as urgentes FROM notificacion where estado='1'; SELECT COUNT(*) as progresos FROM notificacion where estado='2';", function (err, rows) {
        if (err) { throw err; } else {
            data["resUrg"] = rows[0];
            data["resProc"] = rows[1];
            res.json(data);
        }
    });
});


//Handlebars
var hbs = exphbs.create({});
hbs.handlebars.registerHelper('est', function (value) {
    return value === 2;
});
hbs.handlebars.registerHelper('notexist', function (value) {
    return value === 'vacio';
});
hbs.handlebars.registerHelper('estado_usuario', function (value) {
    return value === 1;
});
hbs.handlebars.registerHelper('supervisor', function (value) {
    return value === 2;
});
hbs.handlebars.registerHelper('administrador', function (value) {
    return value === 1;
});